# สถานการณ์ โควิด-19

รายงานสถานการณ์ โควิด-19 จากเว็ของกรมควบคุมโรค (https://covid19.ddc.moph.go.th) ผ่าน API https://covid19.ddc.moph.go.th/th/api 

## ดาวน์โหลด

[ดาวน์โหลด ที่นี่](https://gitlab.com/siteslave/covid-dashboard-app/-/raw/master/apk/app-release.apk)

## รายละเอียด

พัฒนาด้วย Flutter มีเฉพาะโค้ดที่จำเป็นนะครับ ให้สร้าง project ใหม่แล้ว เอา libs, pubspec.yaml ไปใช้งานต่อนะครับ

```
git clone https://gitlab.com/siteslave/covid-dashboard-app.git
```

## อัปเดท

* version 1.0.4 (27/04/2563 11:58) ([ดาวน์โหลด](https://gitlab.com/siteslave/covid-dashboard-app/-/raw/master/apk/app-release.apk))
  - ดึงข้อมูลภาพรวมทั่วโลกจาก `https://github.com/CSSEGISandData/COVID-19` 
  - อัปเดท flutter เป็นเวอร์ชัน `1.17.0-3.2.pre`
## ตัวอย่าง

![](ss/ss1.jpeg)