import 'package:covid_dashboard/api.dart';
import 'package:covid_dashboard/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert' as convert;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'dart:io';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  List<double> chartData = [0];
  Api api = Api();
  Helper helper = Helper();

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  List provinces = [];

  int Confirmed = 0;
  int Recovered = 0;
  int Hospitalized = 0;
  int Deaths = 0;
  int NewConfirmed = 0;
  int NewRecovered = 0;
  int NewHospitalized = 0;
  int NewDeaths = 0;
  String UpdateDate = '';

  int GNewConfirmed = 0;
  int GTotalConfirmed = 0;
  int GNewDeaths = 0;
  int GTotalDeaths = 0;
  int GNewRecovered = 0;
  int GTotalRecovered = 0;
  String GUpdated = '';

  bool loading = false;
  bool isRefresh = false;
  bool showInfo = false;

  openDDC() async {
    const url = 'https://covid19.ddc.moph.go.th/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future getWorld() async {
    setState(() {
      loading = isRefresh ? false : true;
    });
    try {
      var rs = await api.getWorld();

      setState(() {
        loading = false;
      });

      if (rs.statusCode == 200) {
        var jsonDecoded = convert.jsonDecode(rs.body);
        setState(() {
          GTotalConfirmed = jsonDecoded['Confirmed'];
          GTotalRecovered = jsonDecoded['Recovered'];
          GTotalDeaths = jsonDecoded['Deaths'];
          GNewConfirmed = jsonDecoded['NewConfirmed'];
          GNewDeaths = jsonDecoded['NewDeaths'];
          GNewRecovered = jsonDecoded['NewRecovered'];
          GUpdated = jsonDecoded['updated'];
        });
      } else {
        helper.alert('เกิดข้อผิดพลาด [500]');
      }
    } catch (error) {
      setState(() {
        loading = false;
      });

      helper.alert('เกิดข้อผิดพลาด [local]');
      print(error);
    }
  }

  Future getToday() async {
    setState(() {
      loading = isRefresh ? false : true;
    });
    try {
      var rs = await api.getToday();

      setState(() {
        loading = false;
      });
      if (rs.statusCode == 200) {
        var jsonDecoded = convert.jsonDecode(rs.body);
        setState(() {
          Confirmed = jsonDecoded['Confirmed'];
          Recovered = jsonDecoded['Recovered'];
          Hospitalized = jsonDecoded['Hospitalized'];
          Deaths = jsonDecoded['Deaths'];
          NewConfirmed = jsonDecoded['NewConfirmed'];
          NewRecovered = jsonDecoded['NewRecovered'];
          NewHospitalized = jsonDecoded['NewHospitalized'];
          NewDeaths = jsonDecoded['NewDeaths'];
          UpdateDate = jsonDecoded['UpdateDate'];
        });
      } else {
        helper.alert('เกิดข้อผิดพลาด [500]');
      }
    } catch (error) {
      setState(() {
        loading = false;
      });

      helper.alert('เกิดข้อผิดพลาด [local]');
      print(error);
    }
  }

  Future getTimeline() async {
    setState(() {
      loading = isRefresh ? false : true;
    });
    try {
      var rs = await api.getTimeline();

      setState(() {
        loading = false;
      });
      if (rs.statusCode == 200) {
        var jsonDecoded = convert.jsonDecode(rs.body);
        List data = jsonDecoded['Data'];
        List _data = data.reversed.toList().take(10).toList();

        // print(_data);
        var __data = _data.reversed.toList();

        setState(() {
          chartData = [];
        });

        __data.forEach((e) {
          double x = double.tryParse(e['NewConfirmed'].toString()) ?? 0;
          setState(() {
            chartData.add(x);
          });
        });
      } else {
        helper.alert('เกิดข้อผิดพลาด [500]');
      }
    } catch (error) {
      setState(() {
        loading = false;
      });

      helper.alert('เกิดข้อผิดพลาด [local]');
      print(error);
    }
  }

  Future getSum() async {
    setState(() {
      loading = isRefresh ? false : true;
    });
    try {
      var rs = await api.getSum();

      setState(() {
        loading = false;
      });
      if (rs.statusCode == 200) {
        var jsonDecoded = convert.jsonDecode(rs.body);
        Map data = jsonDecoded['Province'];
        int total = 0;
        setState(() {
          provinces = [];
        });

        data.forEach((key, value) {
          if (total < 10) {
            Map x = {"name": key, "total": value};
            setState(() {
              provinces.add(x);
            });
            total++;
          }
        });
      } else {
        helper.alert('เกิดข้อผิดพลาด [500]');
      }
    } catch (error) {
      setState(() {
        loading = false;
      });

      helper.alert('เกิดข้อผิดพลาด [local]');
      print(error);
    }
  }

  Future getAll() async {
    await getToday();
    await getTimeline();
    await getSum();
    await getWorld();
  }

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      isRefresh = true;
    });
    await getAll();
    setState(() {
      isRefresh = false;
    });

    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    if (mounted) _refreshController.loadComplete();
  }

  showAlertNotConnected() {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('No internet connection'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Icon(Icons.cloud_off, size: 50, color: Colors.grey),
                Text(
                  'ตรวจสอบการเชื่อมต่ออินเตอร์เน็ต',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ปิดแอป',
                  style: TextStyle(
                      color: Colors.red, fontWeight: FontWeight.bold)),
              onPressed: () {
                // Navigator.of(context).pop();
                exit(0);
              },
            ),
          ],
        );
      },
    );
  }

  checkInternetConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('Connected');
        getAll();
      }
    } on SocketException catch (_) {
      print('not connected');
      showAlertNotConnected();
    }
  }

  @override
  void initState() {
    super.initState();
    checkInternetConnection();
  }

  @override
  Widget build(BuildContext context) {
    Widget buildGlogal() {
      return Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 10),
                    margin: EdgeInsets.only(left: 5, right: 5),
                    height: 140,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'ผู้ป่วยรายใหม่',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                        Text('${helper.formatNumber(GNewConfirmed, 0)}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 30)),
                        Text(
                          'คน',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.pink,
                        border: Border.all(width: 2, color: Colors.white),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 10),
                    margin: EdgeInsets.only(left: 5, right: 5),
                    height: 140,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'ผู้ป่วยสะสม',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color(0xff00363a),
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                        Text('${helper.formatNumber(GTotalConfirmed, 0)}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xff00363a),
                                fontWeight: FontWeight.bold,
                                fontSize: 30)),
                        Text(
                          'คน',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color(0xff00363a),
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.amber,
                        border: Border.all(width: 2, color: Colors.white),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                )
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 10),
                    margin: EdgeInsets.only(left: 5, right: 5),
                    height: 140,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'รักษาหาย',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                        Text('${helper.formatNumber(GTotalRecovered, 0)}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 30)),
                        Text(
                          '+${helper.formatNumber(GNewRecovered, 0)}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.green,
                        border: Border.all(width: 2, color: Colors.white),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 10),
                    margin: EdgeInsets.only(left: 5, right: 5),
                    height: 140,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'เสียชีวิต',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color(0xff00363a),
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                        Text('${helper.formatNumber(GTotalDeaths, 0)}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xff00363a),
                                fontWeight: FontWeight.bold,
                                fontSize: 30)),
                        Text(
                          '+${helper.formatNumber(GNewDeaths, 0)}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color(0xff00363a),
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.red,
                        border: Border.all(width: 2, color: Colors.white),
                        borderRadius: BorderRadius.circular(20)),
                  ),
                )
              ],
            )
          ],
        ),
      );
    }

    List<Widget> buildWidgets() {
      return provinces.map((e) {
        return Container(
            margin: EdgeInsets.only(top: 8, left: 10, right: 10),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: ListTile(
                leading: CircleAvatar(
                    child: FaIcon(FontAwesomeIcons.userAstronaut,
                        color: Colors.teal),
                    backgroundColor: Colors.teal[50]),
                title: Text('${e['name']}',
                    style: TextStyle(fontSize: 18, color: Colors.teal[900])),
                trailing: Container(
                  padding: EdgeInsets.only(left: 4, right: 4),
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(5)),
                  child: Text(helper.formatNumber(e['total'], 0),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 16)),
                )));
      }).toList();
    }

    return Scaffold(
        appBar: AppBar(
          // leading: CircleAvatar(
          //   radius: 5,
          //   backgroundColor: Colors.grey[50],
          //   backgroundImage: AssetImage('assets/images/bacteria.png'),
          // ),
          leading: Image(
            width: 40,
            image: AssetImage('assets/images/bacteria.png'),
          ),
          elevation: 0,
          title: Text(
            'สถานการณ์โรค COVID-19',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          // actions: <Widget>[
          //   IconButton(
          //     icon: Icon(Icons.refresh, color: Colors.white, size: 25),
          //     onPressed: () => getAll(),
          //   )
          // ],
          backgroundColor: Colors.teal[800],
        ),
        backgroundColor: Colors.teal[800],
        body: ModalProgressHUD(
          color: Colors.teal,
          progressIndicator: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.teal),
          ),
          inAsyncCall: loading,
          child: SmartRefresher(
            enablePullDown: true,
            enablePullUp: false,
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            header: WaterDropHeader(
              complete: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.check, color: Colors.white),
                  SizedBox(width: 10),
                  Text('ดึงข้อมูลเสร็จแล้ว',
                      style: TextStyle(color: Colors.white)),
                ],
              ),
              refresh: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    ),
                  ),
                  Text('กรุณารอซักครู่...',
                      style: TextStyle(color: Colors.white)),
                ],
              ),
              waterDropColor: Colors.white,
              idleIcon: Icon(Icons.refresh, color: Colors.teal),
            ),
            child: ListView(
              // padding: EdgeInsets.only(top: 10),
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          )),
                      padding:
                          const EdgeInsets.only(top: 10, left: 0, right: 0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(right: 10, left: 10),
                                  height: 140,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text('รายใหม่',
                                          style: TextStyle(
                                              color: Color(0xff00363a),
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16)),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          NewConfirmed == 0
                                              ? Container()
                                              : Icon(
                                                  Icons.arrow_upward,
                                                  color: Colors.green,
                                                  size: 20,
                                                ),
                                          Text(
                                              '${helper.formatNumber(NewConfirmed, 0)}',
                                              style: TextStyle(
                                                  color: Color(0xff00363a),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 35)),
                                        ],
                                      ),
                                      Container(
                                        height: 50,
                                        padding: EdgeInsets.only(bottom: 8),
                                        child: Sparkline(
                                          data: chartData,
                                          lineWidth: 2,
                                          fillMode: FillMode.below,
                                          fillGradient: new LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            colors: [
                                              Colors.teal[200],
                                              Colors.white
                                            ],
                                          ),
                                          fillColor: Colors.teal[200],
                                          lineGradient: new LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            colors: [
                                              Colors.teal[800],
                                              Colors.teal[200]
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      border: Border.all(
                                          width: 2, color: Colors.white),
                                      color: Colors.white),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.only(top: 10),
                                  margin: EdgeInsets.only(left: 5, right: 10),
                                  height: 140,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'ผู้ป่วยสะสม',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Color(0xff00363a),
                                            fontWeight: FontWeight.normal,
                                            fontSize: 16),
                                      ),
                                      Text(
                                          '${helper.formatNumber(Confirmed, 0)}',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Color(0xff00363a),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 35)),
                                      Text(
                                        'คน',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Color(0xff00363a),
                                            fontWeight: FontWeight.normal,
                                            fontSize: 16),
                                      ),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.amber,
                                      border: Border.all(
                                          width: 2, color: Colors.white),
                                      borderRadius: BorderRadius.circular(20)),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15, right: 10, top: 5, bottom: 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Icon(Icons.notifications_active,
                                        color: Colors.teal),
                                    Text('สถานการณ์ประจำวัน ',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 20,
                                          color: Colors.teal[900],
                                        )),
                                    Text('($UpdateDate น.)',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.grey)),
                                  ],
                                ),
                                // CircleAvatar(
                                //   backgroundColor: Colors.white,
                                //   radius: 15,
                                //   child: IconButton(
                                //     icon: FaIcon(
                                //       FontAwesomeIcons.redo,
                                //       size: 15,
                                //       color: Colors.green,
                                //     ),
                                //     onPressed: () => getAll(),
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 10, right: 10),
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(bottom: 5),
                                        decoration: BoxDecoration(
                                            color: Colors.green[50],
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: ListTile(
                                          title: Text('รักษาหาย',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 16,
                                                  color: Colors.grey)),
                                          subtitle: Text(
                                              '${helper.formatNumber(Recovered, 0)}',
                                              style: TextStyle(
                                                  fontSize: 25,
                                                  color: Color(0xff00363a),
                                                  fontWeight: FontWeight.bold)),
                                          trailing: Text(
                                            '+${helper.formatNumber(NewRecovered, 0)}',
                                            style: TextStyle(
                                                color: Colors.green,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          leading: CircleAvatar(
                                            backgroundColor: Colors.green,
                                            child: FaIcon(
                                                FontAwesomeIcons.smileWink,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                            color: Colors.red[50],
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: ListTile(
                                          title: Text('เสียชีวิต',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 16,
                                                  color: Colors.grey)),
                                          subtitle: Text(
                                              '${helper.formatNumber(Deaths, 0)}',
                                              style: TextStyle(
                                                  fontSize: 25,
                                                  color: Color(0xff00363a),
                                                  fontWeight: FontWeight.bold)),
                                          trailing: Text(
                                            NewDeaths > 0
                                                ? '+${helper.formatNumber(NewDeaths, 0)}'
                                                : '0',
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          leading: CircleAvatar(
                                            backgroundColor: Colors.red,
                                            child: FaIcon(
                                                FontAwesomeIcons.bookDead,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 5),
                                        decoration: BoxDecoration(
                                            color: Colors.teal[50],
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: ListTile(
                                          title: Text('รักษาตัวที่โรงพยาบาล',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 16,
                                                  color: Colors.grey)),
                                          subtitle: Text(
                                              '${helper.formatNumber(Hospitalized, 0)}',
                                              style: TextStyle(
                                                  fontSize: 25,
                                                  color: Color(0xff00363a),
                                                  fontWeight: FontWeight.bold)),
                                          trailing: Text(
                                            '${helper.formatNumber(NewHospitalized, 0)}',
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          leading: CircleAvatar(
                                            backgroundColor: Colors.teal,
                                            child: FaIcon(
                                                FontAwesomeIcons.hospital,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                FaIcon(FontAwesomeIcons.globe,
                                    color: Colors.teal),
                                SizedBox(
                                  width: 10,
                                ),
                                Text('สถานการณ์ทั่วโลก',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20,
                                      color: Colors.teal[900],
                                    )),
                              ],
                            ),
                          ),
                          buildGlogal(),
                          Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  width: 1,
                                  color: Colors.grey[400],
                                ),
                                borderRadius: BorderRadius.circular(10)),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                      'https://github.com/CSSEGISandData/COVID-19 (Johns Hopkins)',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 11)),
                                  Text('วันที่ $GUpdated (มีการอัปเดทตลอดเวลา)',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 11)),
                                  Text(
                                      '(การอัปเดทขึ้นกับ timezone ของแต่ละพื้นที่)',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 11)),
                                ]),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 10, top: 5),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.pie_chart,
                                      color: Colors.teal,
                                    ),
                                    SizedBox(width: 10),
                                    Text('10 อันดับจังหวัด ',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 20,
                                          color: Colors.teal[900],
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          provinces.length > 0
                              ? Column(
                                  children: buildWidgets(),
                                )
                              : Container(),
                          GestureDetector(
                            onTap: () => openDDC(),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                    child: Container(
                                  margin: EdgeInsets.only(
                                      top: 10, left: 10, right: 10, bottom: 10),
                                  height: 150,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/coronavirus-banner-moph.jpg'),
                                          fit: BoxFit.cover),
                                      color: Colors.teal[900],
                                      border: Border.all(
                                          color: Colors.white, width: 5),
                                      borderRadius: BorderRadius.circular(20)),
                                  child: null,
                                ))
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('COVID-19 DASHBOARD (v1.0.4)',
                                  style: TextStyle(color: Colors.pink)),
                              IconButton(
                                icon: Icon(Icons.info, color: Colors.pink),
                                onPressed: () {
                                  setState(() {
                                    showInfo = !showInfo;
                                  });
                                },
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text('by Satit Rianpit (rianpit@gmail.com)',
                                style: TextStyle(color: Colors.grey)),
                          ),
                          showInfo
                              ? Container(
                                  child: Column(
                                    children: <Widget>[
                                      Divider(),
                                      Text(
                                          'Thai: https://covid19.ddc.moph.go.th/th/api',
                                          style: TextStyle(color: Colors.grey)),
                                      Text(
                                          'world: https://github.com/CSSEGISandData/COVID-19',
                                          style: TextStyle(color: Colors.grey)),
                                      Text('icon: https://www.flaticon.com/',
                                          style: TextStyle(color: Colors.grey)),
                                    ],
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
