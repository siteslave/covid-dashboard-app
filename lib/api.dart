import 'package:http/http.dart' as http;
import 'dart:async';

class Api {
  Api();

  Future<http.Response> getToday() async {
    var url = 'https://covid19.th-stat.com/api/open/today';

    return http.get(url);
  }

  Future<http.Response> getTimeline() async {
    var url = 'https://covid19.th-stat.com/api/open/timeline';

    return http.get(url);
  }

  Future<http.Response> getSum() async {
    var url = 'https://covid19.th-stat.com/api/open/cases/sum';

    return http.get(url);
  }

  Future<http.Response> getWorld() async {
    var url =
        'https://r7app.moph.go.th/api/covid-risk-survey/stat/covid/global';

    return http.get(url);
  }

  // Future<http.Response> getWorld() async {
  //   var url = 'https://api.covid19api.com/summary';

  //   return http.get(url);
  // }
}
